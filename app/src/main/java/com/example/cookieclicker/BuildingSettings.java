package com.example.cookieclicker;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class BuildingSettings implements View.OnClickListener{
    public String name;
    private float cost;
    private float incr;
    private IncrementManager increment_manager;
    private TextView cost_view;
    private TextView incr_view;
    private ImageButton button;
    private MainActivity activity;
    private String cost_key;

    public BuildingSettings(String name, float cost, float incr, TextView cost_view, TextView incr_view, ImageButton button, MainActivity activity){
        this.activity = activity;
        this.name = name;
        this.cost_key = name + "_COST";
        this.cost = cost;
        this.incr = incr;
        this.load_data();
        this.cost_view = cost_view;
        this.incr_view = incr_view;
        this.show_settings();
        this.increment_manager = null;
        this.button = button;

    }

    public void setIncrement_manager(IncrementManager increment_manager){
        this.increment_manager = increment_manager;
    }

    public void show_settings(){
        this.cost_view.setText(String.format("%.1f", this.cost));
        this.incr_view.setText(String.format("+%.1f", this.incr));
    }

    public void increment_cost(){
        this.cost = this.cost * (float) 1.5;
        this.activity.pref_put_float(this.cost_key, this.cost);
        this.show_settings();
    }

    @Override
    public void onClick(View v) {
        this.increment_manager.add_incr_per_sec(this.incr);
        this.increment_manager.increment(-this.cost);
        this.increment_cost();
    }

    public void update_availability(float in_bank){
        this.button.setEnabled(in_bank >= this.cost);
        if (in_bank >= this.cost){
            this.button.setAlpha(255);
        } else {
            this.button.setAlpha(100);
        }
    }

    private void load_data() {
        float cost = this.activity.pref_load_float(this.cost_key);
        if (cost > 0) {
            this.cost = cost;
        }
    }
}
