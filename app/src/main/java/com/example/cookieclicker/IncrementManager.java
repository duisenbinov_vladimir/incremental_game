package com.example.cookieclicker;

import android.os.Handler;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class IncrementManager {
    private float in_bank;
    private float incr_per_sec;
    TextView in_bank_view;
    TextView incr_rate_view;
    ArrayList<BuildingSettings> building_buttons;
    private MainActivity activity;
    private String IN_BANK_KEY = "IN_BANK";
    private String INCR_KEY = "INCR";
    private String LAST_UPDATE_DATE = "LAST_DATE";
    private String DATE_PATTERN = "yyyyMMdd_HHmmss";

    public IncrementManager(TextView in_bank_view, TextView incr_rate_view, ArrayList<BuildingSettings> building_buttons, MainActivity activity){
//        this.in_bank = 0;
//        this.incr_per_sec = 0;
        this.activity = activity;
        this.load_data();
        this.in_bank_view = in_bank_view;
        this.incr_rate_view = incr_rate_view;
        this.building_buttons = building_buttons;


        final Handler handler = new Handler();
        final int delay = 1000; //milliseconds
        final IncrementManager self = this;

        handler.postDelayed(new Runnable(){
            public void run(){
                self.increment_by_sec();
                handler.postDelayed(this, delay);
            }
        }, delay);
        this.update_availability(this.in_bank);
        this.show_in_bank();
    }

    public synchronized void increment(float incr){
        this.in_bank += incr;
        this.update_availability(this.in_bank);
        String timeStamp = new SimpleDateFormat(this.DATE_PATTERN, Locale.US).format(new Date());
        this.activity.pref_put_float(this.IN_BANK_KEY, this.in_bank);
        this.activity.pref_put_string(this.LAST_UPDATE_DATE, timeStamp);
        this.show_in_bank();
    }

    private synchronized void update_availability(float new_in_bank){
        for (BuildingSettings buildingSettings: this.building_buttons){
            buildingSettings.update_availability(new_in_bank);
        }
    }

    private synchronized void show_in_bank(){
        this.in_bank_view.setText("In Bank: " + String.format("%.1f", this.in_bank));
        this.incr_rate_view.setText(String.format("%.1f", this.incr_per_sec) + "/s");
    }

    private synchronized void increment_by_sec(){
        if (this.incr_per_sec > 0){
            this.increment(this.incr_per_sec);
        }
    }

    synchronized void add_incr_per_sec(float incr){
        this.incr_per_sec += incr;
        this.activity.pref_put_float(this.INCR_KEY, this.incr_per_sec);
    }

    private synchronized void load_data(){
        float incr = this.activity.pref_load_float(this.INCR_KEY);
        if (incr < 0) {
            this.incr_per_sec = 0;
        } else {
            this.incr_per_sec = incr;
        }

        float in_bank = this.activity.pref_load_float(this.IN_BANK_KEY);
        if (in_bank < 0) {
            this.in_bank = 0;
        } else {
            String last_update_str = this.activity.pref_load_string(this.LAST_UPDATE_DATE);
            try {
                Date last_update = new SimpleDateFormat(this.DATE_PATTERN, Locale.US).parse(last_update_str);
                Date now = new Date();
                int seconds = (int)(now.getTime()-last_update.getTime())/1000;
                Log.i("seccs", seconds+"");
                in_bank += seconds*this.incr_per_sec;
            } catch (Exception e) {
                Log.i("read date error", e.toString());
            }
            this.in_bank = in_bank;
        }

    }

}
