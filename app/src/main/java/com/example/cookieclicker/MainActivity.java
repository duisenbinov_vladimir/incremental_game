package com.example.cookieclicker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public int numberOfClicks = 0;
    public SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = this;
        this.sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);

        ImageButton cookie = findViewById(R.id.cookie);
        final TextView in_bank_view = (TextView) findViewById(R.id.numberOfClicks);
        final TextView incr_rate_view = (TextView) findViewById(R.id.incrementRate);

        ImageButton cursor_button = findViewById(R.id.cursor);
        ImageButton grandma_button = findViewById(R.id.grandma);
        ImageButton factory_button = findViewById(R.id.factory);

        final TextView cursor_cost_view = findViewById(R.id.cursor_cost);
        final TextView cursor_incr_view = findViewById(R.id.cursor_incr);

        final TextView grandma_cost_view = findViewById(R.id.grandma_cost);
        final TextView grandma_incr_view = findViewById(R.id.grandma_incr);

        final TextView factory_cost_view = findViewById(R.id.factory_cost);
        final TextView factory_incr_view = findViewById(R.id.factory_incr);

        BuildingSettings cursor = new BuildingSettings("cursor", 10, 1, cursor_cost_view, cursor_incr_view, cursor_button, this);
        BuildingSettings grandma = new BuildingSettings("grandma", 100, 8, grandma_cost_view, grandma_incr_view, grandma_button, this);
        BuildingSettings factory = new BuildingSettings("factory", 1000, 60, factory_cost_view, factory_incr_view, factory_button, this);

        cursor_button.setOnClickListener(cursor);
        grandma_button.setOnClickListener(grandma);
        factory_button.setOnClickListener(factory);

        ArrayList<BuildingSettings> building_buttons = new ArrayList<BuildingSettings>();
        building_buttons.add(cursor);
        building_buttons.add(grandma);
        building_buttons.add(factory);

        final IncrementManager increment_manager = new IncrementManager(in_bank_view, incr_rate_view, building_buttons, this);

        cookie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increment_manager.increment(1);
            }
        });

        cursor.setIncrement_manager(increment_manager);
        grandma.setIncrement_manager(increment_manager);
        factory.setIncrement_manager(increment_manager);

    }

    public void pref_put_float(String key, float value){
//        Context context = this;
//        SharedPreferences sharedPref = context.getSharedPreferences(
//                getString(R.string.preference_file_key),
//                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public void pref_put_string(String key, String value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public float pref_load_float(String key){
//        Context context = this;
//        SharedPreferences sharedPref = context.getSharedPreferences(
//                getString(R.string.preference_file_key),
//                Context.MODE_PRIVATE);
        return sharedPref.getFloat(key,-1);
    }

    public String pref_load_string(String key){
//        Context context = this;
//        SharedPreferences sharedPref = context.getSharedPreferences(
//                getString(R.string.preference_file_key),
//                Context.MODE_PRIVATE);
        return sharedPref.getString(key,"");
    }
}
